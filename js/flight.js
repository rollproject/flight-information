var departureOrArrival = '';
var isFlightDetail = false;

function initApp() {
 bb.pushScreen('main.html', 'main');
}

function hammerTime() {
  var contentSwipe = new Hammer(document.getElementById("content"), {});

  contentSwipe.onswipe = function(ev) { 
    var dir = ev.direction;
    
    // swipe from right-to-left
    if (dir === 'left') {
      var currentPage = pageStack[pageStack.length -1];
          currentPage = parseInt(currentPage);
      var nextPage = currentPage + 1;

      if (nextPage >= pages.length + 1) {
        return false;
      } else {
        console.log('loading: ' + nextPage);
        bb.pushScreen(nextPage + '.html', nextPage);
      }

    // swipe from left-to-right
    }  else if (dir === 'right') {
        bb.popScreen()
      }

  };
}



function onResume(){
  blackberry.event.removeEventListener("onPause",onPause);
  window.setInterval(getFlightStatus,15000);
}

function onPause(){
  blackberry.event.removeEventListener("onResume",onResume);
   window.setInterval(getFlightStatus,15000);
}

function loadFLightBackgroundProcess(){
}

/*================== CEK FLIGHT STATUS AND NOTIFICATION ======================*/

function getFlightStatus(){
    var urlGetStatus = JSON.parse(localStorage.getItem("urlgetFlightStatus"));
    var tempflightno = [];
    console.log(urlGetStatus.url);
    tempflightno = JSON.parse(localStorage.getItem("flightSavedStorage"));
    
    //console.log(tempflightno);
    var postflightno = [];
    if(tempflightno){
        for(var a = 0;  a < tempflightno.length ; a++){
        postflightno.push(tempflightno[a].idflightNo); }
    
        for(var a = 0 ; a< urlGetStatus.url.length; a++){
            console.log(postflightno);
            $.ajax({
              type : 'POST',
              data: { id_flight : JSON.stringify(postflightno), TOKEN : "3b0ce003959c28762881c746fe8e100e6ea8e4ea"},
              url : urlGetStatus.url[a]._url,
              dataType : 'json',
              timeout: 5000,
              success : function(data) {
                  //alert(data);
                  flightcheckstatus(data);
                  console.log("data flight status : ",data);
              },error:function(error){}
    });}}

    /*end function*/
  }

function flightcheckstatus(data){
    var nowflightstatus = JSON.parse(localStorage.getItem('flightSavedStorage'));
    for(var a = 0; a< data.length ; a++){
        for(var i = 0; i < nowflightstatus.length ; i++){
            if(data[a].status != ""){
                if(data[a].flightno == nowflightstatus[i].idflightNo){
                    console.log('data1 : ',data[a].status);
                    console.log('data2 : ',nowflightstatus[i].idstatus);
                    if(data[a].status != nowflightstatus[i].idstatus){
                            console.log("tidak sama");
                            store_notification(data[a]);
                            nowflightstatus[i].idstatus = data[a].status;
                            localStorage.setItem("flightSavedStorage",JSON.stringify(nowflightstatus));
                            break;
                            //console.log(data[a].status,"blablabla")
                    }else{
                      console.log("sama");
                    }}}}}
  
    if(localStorage.getItem("storageNotification")){
              console.log("page",localStorage.getItem("page"));
              var ll = JSON.parse(localStorage.getItem('storageNotification'));
              console.log("lenght",JSON.parse(localStorage.getItem('storageNotification')).length);
              console.log("dialog keluar",ll);
          }
          
    /*end function*/
  }

function showDialogNotification(){
  
    $("#tableFlightContent").html('');
    var notifAlert = JSON.parse(localStorage.getItem('storageNotification'));
    for(var a = 0 ; a < notifAlert.length;a++){
            $("<div/>",{"id":"notif"+a,"class":"formNotif"}).appendTo("#tableFlightContent");
            $("<font/>",{"id":"flightno"+a,"class":"flightNo"}).text(notifAlert[a].flightid).appendTo("#notif"+a);
            $("<font/>",{"id":"status"+a,"class":"status"}).css({color:setColorStatus(notifAlert[a].status)}).text(notifAlert[a].status).appendTo("#notif"+a); }
     localStorage.setItem('page','listNotification.html');
     
     /*end function*/
  } 
 
function store_notification(data){

    console.log('untuk notification',JSON.parse(localStorage.getItem("storageNotification")));
    var tempStore = JSON.parse(localStorage.getItem("storageNotification"));  
    var tempNotif = {
          flightid : data.flightno,
          status : data.status   };

    try{
      
      if(JSON.parse(localStorage.getItem('storageNotification')).length > 0){  
            console.log('punya tempStore',tempStore);
            console.log('data storageNotification',localStorage.getItem("storageNotification")); 
            var isSame = false;
            for(var a = 0; a< tempStore.length;a++){
                
                if(tempStore[a].flightid == data.flightno && tempStore[a].status != data.status){
                    tempStore.push(tempNotif);
                    //tempStore[a].status = tempNotif.status;
                    console.log(localStorage.getItem("page"));
                    tempStore.splice(a,1);
                    localStorage.setItem("storageNotification",JSON.stringify(tempStore)); 
                    console.log('sekarang notifikasinya',localStorage.getItem("storageNotification"));      
                    ifNotifPageIsload();
                    isSame = true;
                    a = tempStore.length;
                    break;
                }else{
                  isSame = false;
                }}
    
            /* ini kalo tidak ada flight id di storage notifikasi maka akan di alihkan ke sini untuk dimasukan ke notifikasi 
            sebagai data baru*/
            if(!isSame){
                console.log("toSave",tempNotif);
                var toSave = [];
                //toSave.push(tempNotif);
                tempStore.push(tempNotif);
                localStorage.setItem("storageNotification",JSON.stringify(tempStore));
                callNotificationService();
                ifNotifPageIsload();
            }

      }else{
        localStorage.setItem("page",'index.html');+
        console.log("toSave",tempNotif);
        var toSave = [];
        toSave.push(tempNotif);
        localStorage.setItem("storageNotification",JSON.stringify(toSave));
        callNotificationService();
        ifNotifPageIsload(); }
          
    }catch(err){
      console.log('proses notifikasi',err);
      localStorage.setItem("page",'index.html');
      console.log("toSave",tempNotif);
      var toSave = [];
      toSave.push(tempNotif);
      localStorage.setItem("storageNotification",JSON.stringify(toSave));
      callNotificationService();
      ifNotifPageIsload();
    }
  
}

function ifNotifPageIsload(){

  if(localStorage.getItem("page") != "listNotification.html"){
     flightNotificationForm();
  }else{
    console.log("enggak perlu buka list notification",localStorage.getItem('page'));
    showDialogNotification();
  }
}

function callNotificationService(){

  try{

    blackberry.event.addEventListener("invoked", onInvoked);
    // Handle invoked event
    function onInvoked(onInvokedInfo) {
         // Do something if the action is "BB.action.DoSomethingForNotification"
         if(onInvokedInfo.action == "BB.action.DoSomethingForNotification") {
             doSomething(onInvokedInfo.uri); // onInvokedInfo.uri is "some link"
        }
    }
    
    // Create a notification with invocation information that invokes other application
    var title = "Indonesia Airport Notification";
    var noftToService = "";
        var tempNoftToService = JSON.parse(localStorage.getItem("storageNotification"));
        
        for(var a = 0; a< tempNoftToService.length;a++){
          noftToService += 'Flight No : '+tempNoftToService[a].flightid+' = '+tempNoftToService[a].status+"\n";
          console.log('avfvf',noftToService);
        }

    var options = {
        body : noftToService,
        target : "sys.browser",
        targetAction : "bb.action.OPEN",
        payloadType : "text/html",
        payloadURI : "the link"
    }

    // Create the notification
    // and when the user opens the notification item in UIB, it will invoke browser with "the link"
    new Notification(title, options);
    navigator.vibrate(1000);

  }catch(err){
    console.log(err);
  } 
  
}

/* END FLIGHT STATUS AND NOTIFICATION */


function flightsaved(){
 bb.pushScreen("flightsaved.html","flightsaved.html");
}

function flightNotificationForm(){
  bb.pushScreen("listNotification.html","listNotification.html");
}

function setPlacePage(data){
  localStorage.setItem('page','index.html');
  localStorage.removeItem("storageNotification");
}

function intentTo(ev,optionUrl,optionUrlContent){
  titleHeaderFlight = 'Flight Arrival Domestic';
  isMore = false;
  kel = 20;
  localStorage.removeItem('moreFilterFlightArrival'); 
  localStorage.removeItem('moreFilterFlightDeparture'); 
  localStorage.removeItem('storeFlightDeparture');
  localStorage.removeItem('storeFlightArrival');
  set_filtered.isFiltered = false;
  set_filtered.optionFilteredStatus = 'none';
  localStorage.setItem('numberFlight',1);
  if(optionUrl == "baliairport"){
   // titleHeaderFlight =
    localStorage.setItem('urlGetFlight',baliairport);
  }else{
    localStorage.setItem('urlGetFlight',surabayaairport);
  }
  localStorage.setItem('airportIs',optionUrl);
  localStorage.setItem('urlContentFlight',optionUrlContent); 
  if(ev){
    bb.pushScreen(ev,ev);
  }else{
  }
  console.log(localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight'));
  
  /*end function*/
};

function sub_intentTo(ev,optionUrlContent,number,jenis_penerbangan){
  titleHeaderFlight = 'Flight '+$('#menuFligh'+number).text();
  
 // titleHeaderFlight = jenis_penerbangan;
  isMore = false;
  kel = 20;
  localStorage.removeItem('moreFilterFlightArrival'); 
  localStorage.removeItem('moreFilterFlightDeparture'); 
  localStorage.removeItem('storeFlightDeparture');
  localStorage.removeItem('storeFlightArrival');
  set_filtered.isFiltered = false;
  set_filtered.optionFilteredStatus = 'none';
  localStorage.setItem('numberFlight',number);
  localStorage.setItem('urlContentFlight',optionUrlContent); 
  if(ev){
    bb.pushScreen(ev,ev);
  }else{
  }
  console.log(localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight'));
};

function flightFilter_initialLoad (element){
    element.getElementById('flightstatus').value = set_filtered.optionFilteredStatus;
    
};

function ke_flight_detail_initialLoad(element){
  // Make the proper activity indicator appear
  $("#status").attr('color',setColorStatus(contentFlightClick.idstatus));
  $('#fromto').text(contentFlightClick.fromto);
  $('#flightno').text(contentFlightClick.idflightNo);
  $('#status').text(contentFlightClick.idstatus);
  $('#gate').text(contentFlightClick.gate);
  $('#eta').text(contentFlightClick.idfontETA);
  $('#reeta').text(contentFlightClick.idfontREETA);
  
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));
  if(flightItem[contentFlightClick.idflightNo.substring(0,2).toLowerCase()] == null){
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem["unknown"]}).appendTo("#imageflight");
    }else{
      $('<img/>',{id:'idimage_flight',class:'image_flight',src:flightItem[contentFlightClick.idflightNo.substring(0,2).toLowerCase()]}).appendTo("#imageflight");
   }
}


function FlightDeparture_initialLoad(element){
  // Make the proper activity indicator appear
  $('.titleFont').text(titleHeaderFlight);
  if (bb.device.isBB10) {
    element.getElementById('bb7Loading').style.display = 'none';
  } else {
    element.getElementById('bb10Loading').style.display = 'none';
  }

    var lengh_data = 20;
    if (set_filtered.isFiltered) {
      lengh_data = 200;
    }

    $.ajax({
        type : 'GET',
        timeout:5000,
        url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'0/'+lengh_data+'/'+token,
        dataType : 'json',
        success : function(data) {
           
            element.getElementById('bb7Loading').style.visibility = 'hidden';
            element.getElementById('bb10Loading').style.visibility = 'hidden';
            element.getElementById('tableFlightContent').style.visibility = 'visible';
            console.log('finish');
            console.log('content:',data);
  
            if(data){
              if(data == ''){
                element.getElementById('infoAlertBox').style.display = 'block';
                element.getElementById('bb7Loading').style.display = 'none';
                element.getElementById('bb10Loading').style.display = 'none';
                
              }else{
                  element.getElementById('tableFlightContent').style.display = 'block';
                  element.getElementById('bb7Loading').style.display = 'none';
                  element.getElementById('bb10Loading').style.display = 'none';
                  element.getElementById('infoAlertBox').style.display = 'none';
                  localStorage.setItem("storeFlightDeparture",JSON.stringify(data));
                  storeDataDeparture(element,data);
              }
            }else{
              element.getElementById('infoAlertBox').style.display = 'block';
              element.getElementById('bb7Loading').style.display = 'none';
              element.getElementById('bb10Loading').style.display = 'none';
            }
        },beforeSend: function(){
         console.log('onRunning');
        },error: function(xhr, status, error) {
          element.getElementById('tableFlightContent').style.display = 'block';
          element.getElementById('tableFlightContent').style.visibility = 'visible';
          document.getElementById('infoAlertBox').style.display = 'block';
          element.getElementById('bb10Loading').style.display = 'none';
        }
    });
}

function FlightArrival_initialLoad(element){
  // Make the proper activity indicator appear
  $('.titleFont').text(titleHeaderFlight);
  if (bb.device.isBB10) {
    element.getElementById('bb7Loading').style.display = 'none';
  } else {
    element.getElementById('bb10Loading').style.display = 'none';
  }

  var lengh_data = 20;
  if (set_filtered.isFiltered) {
    lengh_data = 200;
  }
  
  var text_status = '';
  $.ajax({
    type : 'GET',
    timeout:3000,
    url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'0/'+lengh_data+'/'+token,
    dataType : 'json',
    success : function(data) {
    
        element.getElementById('tableFlightContent').style.display = 'inline';
        element.getElementById('bb10Loading').style.visibility = 'hidden';
        element.getElementById('tableFlightContent').style.visibility = 'visible';
        console.log('finish');
        console.log('content:',data);
        
        if(data){
          if(data == ''){ 
            element.getElementById('infoAlertBox').style.display = 'block';
            //$("#infoAlertBox").show();
          }else{
            //element.getElementById('infoAlertBox').style.display = 'inline';
            element.getElementById('infoAlertBox').style.display = 'none';
            localStorage.setItem("storeFlightArrival",JSON.stringify(data));
            storeDataArrival(element,data);
          }
          
        }else{
          document.getElementById('infoAlertBox').style.display = 'block';
        }
        
    },beforeSend: function(){
      console.log('onRunning');
    },error: function(xhr, status, error) {
      element.getElementById('tableFlightContent').style.display = 'block';
      element.getElementById('tableFlightContent').style.visibility = 'visible';
      document.getElementById('infoAlertBox').style.display = 'block';
      element.getElementById('bb10Loading').style.display = 'none';
    }
  });
  /*end function*/ 
}

function flightSearch_initalLoad(element){
  // Make the proper activity indicator appear
  if (bb.device.isBB10) {
    //element.getElementById('bb7Loading').style.display = 'none';
  } else {
    //element.getElementById('bb10Loading').style.display = 'none';
  }
}

var isMore = false;
function moreFlightDeparture_list(){
  isMore = true;
  kel += 20;
  $("#scrollpane").attr("data-bb-scroll-effect","off");
  $("#disablepage").show();
  $("#loadingdiv").show();

  $.ajax({
      type : 'GET',
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'0/'+kel+'/'+token,
      dataType : 'json',
      success : function(data) {       
          console.log('finish');
          console.log('content:',data);
          $("#disablepage").hide();
          $("#loadingdiv").hide();
          if(data){
            storeDataMoreFlightDeparture(data);
          }else{
          }
      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        $("#disablepage").hide();
        $("#loadingdiv").hide();
        alert(error);
      }
  });
}

function moreFlightArrival_list(){
  kel += 20;
  $("#disablepage").show();
  $("#loadingdiv").show();

  $.ajax({
      type : 'GET',
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'0/'+kel+'/'+token,
      dataType : 'json',
      success : function(data) {       
          console.log('finish');
          console.log('content:',data);
          $("#disablepage").hide();
          $("#loadingdiv").hide();
          if(data){
            storeDataMoreFlightArrival(data);
      
          }else{
            //alert(error);
          }
      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        $("#disablepage").hide();
        $("#loadingdiv").hide();
        alert(error);
      }
  });
}

function FlightSaved_initialLoad(element){
  $('.titleFont').text("Flight Saved");
  if(localStorage.getItem("flightSavedStorage")){
    storeDataFlightSaved(element);
  }else{
    
  }
}


function search(){
  
  $("#disablepage").html('');
  $("#loadingdiv").html('');
  $('<div/>',{id:"disablepage",class:"disablepage"}).css({'position':'relative', 'witdh':'100%','height':'100%','opacity':'0.9','background':'#000'}).appendTo("#screenFlight").hide();
  $('<div/>',{id:"disablefont"}).css({'position':'relative','top':'50%','left':'50%','margin-left':'-100px'}).appendTo("#disablepage");
  $('<font/>',{id:'pulldown',class:'pulldown'}).text('Please Wait..').css({'font-size':'30pt','text-align': 'center',color:'white','font-style':'bold'}).appendTo("#disablefont");
  $("#disablepage").show();
  $("#loadingdiv").show();
  console.log("ggg : ",localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+$("#flightID").val().replace(/ /g,"%20")+'/0/20/'+token);

  if($("#flightID").val() != ''){
    $.ajax({
      type : 'GET',
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+$("#flightID").val().replace(/ /g,"%20")+'/0/20/'+token,
      dataType : 'json',
      success : function(data) {
          
          $("#disablepage").hide();
          $("#loadingdiv").hide();
          console.log('finish');
          console.log('content:',data);
          if(data){
            if(data == ''){
              //document.getElementById('infoAlertBox').style.visibility = 'visible';
            }else{
              console.log("search : ",data);
              storeDataFlightSearch(data);
            }      
          }else{
            //alert(error);
          }
      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        //document.getElementById('infoAlertBox').style.visibility = 'visible';
        $("#disablepage").hide();
        $("#loadingdiv").hide();
        //alert(error);
      }
  });
  }else{
    $("#disablepage").hide();
    $("#loadingdiv").hide();
  }
}

/*
  ====================kategory onScreenReady========================
*/

function flightSearch_onScreenReady(element){
  element.getElementById('menuFligh'+localStorage.getItem("numberFlight")).setAttribute('data-bb-selected','true');
  //$("#menuFligh"+localStorage.getItem("numberFlight")).attr("data-bb-selected","true");        
  /*$.ajax({
      type : 'GET',
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'444'+'/0/20/'+token,
      dataType : 'json',
      success : function(data) {
          element.getElementById('tableFlightContent').style.display = 'inline'; 
          //element.getElementById('bb7Loading').style.visibility = 'hidden';
          //element.getElementById('bb10Loading').style.visibility = 'hidden';
          element.getElementById('tableFlightContent').style.visibility = 'visible';
          console.log('finish');
          console.log('content:',data);

          if(data){
            localStorage.setItem("storeFlightArrival",JSON.stringify(data));
            //storeDataArrival(element,data);
          }else{
            //alert(error);
          }

      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        alert(error);
      }
  });*/
}

function FlightArrival_onScreenReady(element){
  element.getElementById('menuFligh'+localStorage.getItem("numberFlight")).setAttribute('data-bb-selected','true');
}

function flightFilter_onScreenReady(element){
  element.getElementById('flightstatus').value = set_filtered.optionFilteredStatus;
}

function FlightDeparture_onScreenReady(element){
  element.getElementById('menuFligh'+localStorage.getItem("numberFlight")).setAttribute('data-bb-selected','true');
}

function sleep(milliseconds) {
  var start = new Date().getTime();
  while ((new Date().getTime() - start) < milliseconds){
    // Do nothing
  }
}

function FlightSaved_onScreenReady(element){
}

/*
  ====================END onScreenReady========================
*/


/*
  ====================kategory storeData========================
*/



function storeDataFlightSearch(data){
  //edit disini
  $('#tableFlightContent').html('');
  $('#disablepage').html('');
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));

  console.log("data",data);
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));   
  $('<div/>',{id:'loadingdiv',class:'loadingdiv'}).css({witdh:'100px',height:"auto",background:"silver"}).appendTo("#tableFlightContent").hide();
  $('<div/>',{id:'idp',class:'idp',align:'center'}).appendTo('#loadingdiv');
  
  for(var i = 0; i < data.length ; i++){
    
      if (data[i].to == null) {
        data[i].from = 'From '+data[i].from;
      }else{
        data[i].to = 'To '+data[i].to;
      }
        
      data[i].eta = 'ETA '+data[i].eta;
      data[i].reeta = 'REETA '+data[i].reeta;
      $('<div/>',{id:'idtableflight'+i,class:'list_container',onclick:"showDetailFlight('"+i+"',"+JSON.stringify(data[i])+")"}).appendTo("#tableFlightContent"); 
      $('<div/>',{id:'idheadtable'+i,class:'container_text'}).appendTo("#idtableflight"+i);
      if(data[i].to == null){
        $('<font/>',{id:'fromto'+i,class:'fromTo_flightno'}).text(data[i].from).appendTo("#idheadtable"+i);
      }else{
        $('<font/>',{id:'fromto'+i,class:'fromTo_flightno'}).text(data[i].to).appendTo("#idheadtable"+i);
      } 
      $("<font/>",{id:'idflightNo'+i,class:"fromTo_flightno"}).text(data[i].flightno).appendTo("#idheadtable"+i);
      $('<div/>',{id:'idheadtable_status'+i,class:'container_text'}).appendTo("#idtableflight"+i);
      $("<font/>",{id:"idstatus"+i,class:"text_status"}).css({color:setColorStatus(data[i].status)}).text(ifStatusNull(data[i].status)).appendTo("#idheadtable_status"+i);
      $("<br/>").appendTo("#idheadtable_status"+i);
      $('<div/>',{class:"pagar"}).appendTo("#idheadtable_status"+i);
      $("<div/>",{class:"container_eta_reeta",id:"id_container_eta_reeta"+i}).appendTo("#idheadtable_status"+i);
      $("<font/>",{id:"eta"+i,class:"eta_reeta"}).text(data[i].eta).appendTo("#id_container_eta_reeta"+i);
      $("<font/>",{id:"reeta"+i,class:"eta_reeta"}).text(data[i].reeta).appendTo("#id_container_eta_reeta"+i);
      
  }
  
}

function storeDataFlightSaved(element){

  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));
  var data = JSON.parse(localStorage.getItem("flightSavedStorage"));
  console.log("data",data);

  for(var i = 0; i < data.length ; i++){      
      $('<div/>',{id:'idtableflight'+i,class:'tableflight',onclick:"alertDeleteFlight('"+i+"')"}).appendTo("#tableFlightContent"); 
      $('<div/>',{id:'idheadtable'+i,class:'headtable'}).appendTo("#idtableflight"+i); 
      $('<font/>',{id:'fromto'+i,class:'fontFlight'}).text(data[i].fromto).appendTo("#idheadtable"+i);
  
      if(flightItem[data[i].idflightNo.substring(0,2).toLowerCase()] == null){
        $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem["unknown"]}).appendTo("#idtableflight"+i);
      }else{
        $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem[data[i].idflightNo.substring(0,2).toLowerCase()]}).appendTo("#idtableflight"+i);
      }
  
      $("<font/>",{id:'idflightNo'+i,class:"flightNo"}).text(data[i].idflightNo).appendTo("#idtableflight"+i);
      $('<div/>',{id:'iddivgate'+i,class:'divgate'}).appendTo("#idtableflight"+i);
      $('<font/>',{id:"idgate"+i,class:'gate'}).text(data[i].gate).appendTo("#iddivgate"+i);
      $("<font/>",{id:"idstatus"+i,class:"status"}).css({color:setColorStatus(data[i].idstatus)}).text(ifStatusNull(data[i].idstatus)).appendTo("#idtableflight"+i);
      $('<div/>',{id:'idnextButton'+i,class:'nextButton'}).appendTo("#idtableflight"+i);
      $('<img/>',{id:'idbtnnext'+i,class:'btnnext',src:'images/Action_Icons_BlackBerry_10/ic_next.png'}).appendTo("#idnextButton"+i);
      $('<div/>',{id:'idfoottable'+i,class:'foottable'}).appendTo('#idtableflight'+i);
      $('<font/>',{id:'idfontETA'+i,class:'fontFlight'}).text(data[i].idfontETA).appendTo("#idfoottable"+i);
      $('<font/>',{id:'idfontREETA'+i,class:'fontREETA'}).text(data[i].idfontREETA).appendTo("#idfoottable"+i);  
  }
}

//untuk store data departure
function storeDataDeparture(element,data){
  $('.titleFont').text(titleHeaderFlight);
  $("#tableFlightContent").css("overflow","hidden","position","relative");
  $("#menuFligh"+localStorage.getItem("numberFlight")).attr("data-bb-selected","true");
  $('<div/>',{id:"disablepage",class:"disablepage"}).css({'position':'relative', 'width':'100%','display':'block','height':'1130px','margin-left':'auto','margin-right':'auto','opacity':'0.9','background':'#000'}).appendTo("#screenFlight").hide();
  $('<div/>',{id:"disablefont"}).css({'position':'relative','top':'50%','left':'50%','margin-left':'-100px'}).appendTo("#disablepage");
  $('<font/>',{id:'pulldown',class:'pulldown'}).text('Please Wait..').css({'font-size':'30pt','text-align': 'center',color:'white','font-style':'bold'}).appendTo("#disablefont");
  $("#scrollpane").attr("data-bb-scroll-effect","on");
  new filteredStoreFlight(data,
    function(callback){
       if (callback) {
  
          var flightItem = JSON.parse(localStorage.getItem("imageStorage"));   
          $('<div/>',{id:'loadingdiv',class:'loadingdiv'}).css({witdh:'100px',height:"auto",background:"silver"}).appendTo("#tableFlightContent").hide();
          $('<div/>',{id:'idp',class:'idp',align:'center'}).appendTo('#loadingdiv');
        
          for(var i = 0; i < callback.length ; i++){
              callback[i].to = 'TO '+callback[i].to;
              callback[i].eta = 'ETA '+callback[i].eta;
              
              if (callback[i].reeta == '') {
                callback[i].reeta = 'REETA -';
              }else{
                callback[i].reeta = 'REETA '+callback[i].reeta;
              }
              
              $('<div/>',{id:'idtableflight'+i,class:'list_container',onclick:"showDetailFlight('"+i+"',"+JSON.stringify(callback[i])+")"}).appendTo("#tableFlightContent"); 
              $('<div/>',{id:'idheadtable'+i,class:'container_text'}).appendTo("#idtableflight"+i); 
              $('<font/>',{id:'fromto'+i,class:'fromTo_flightno'}).text(callback[i].to).appendTo("#idheadtable"+i);
              $("<font/>",{id:'idflightNo'+i,class:"fromTo_flightno"}).text(callback[i].flightno).appendTo("#idheadtable"+i);
              $('<div/>',{id:'idheadtable_status'+i,class:'container_text'}).appendTo("#idtableflight"+i);
              
              $("<font/>",{id:"idstatus"+i,class:"text_status"}).css({color:setColorStatus(callback[i].status)}).text(ifStatusNull(callback[i].status)).appendTo("#idheadtable_status"+i);
              $("<br/>").appendTo("#idheadtable_status"+i);
              $('<div/>',{class:"pagar"}).appendTo("#idheadtable_status"+i);
              $("<div/>",{class:"container_eta_reeta",id:"id_container_eta_reeta"+i}).appendTo("#idheadtable_status"+i);
              $("<font/>",{id:"eta"+i,class:"eta_reeta"}).text(callback[i].eta).appendTo("#id_container_eta_reeta"+i);
              $("<font/>",{id:"reeta"+i,class:"eta_reeta"}).text(callback[i].reeta).appendTo("#id_container_eta_reeta"+i);
          }
  
  
          var flightitemSaved = JSON.parse(localStorage.getItem("flightSavedStorage"));
          if(flightitemSaved){
              for(var a = 0 ; a < flightitemSaved.length; a++){
              console.log(flightitemSaved[a].idflightNo);
              for(var b =0;b<callback.length;b++){
                  if(flightitemSaved[a].idflightNo == callback[b].flightno){
                    if (callback[b].flightno == contentFlightClick.idflightNo) {
                      if (isFlightDetail) {
                        console.log('inidia',contentFlightClick.div_flightid);
                        /*
                         * untuk menentukan scroll position habis dari menu detail_flight.html
                         * */   
                        var mm = element.getElementById('idtableflight'+(b-1)); //contentFlightClick.div_flightid
                        //element.getElementById('screenFlight').scrollTo(100,0);
                        element.getElementById('scrollPanel').scrollToElement(mm);
                        isFlightDetail = false;
                      }
                    }
                  $('#idtableflight'+b).hide();
                  }
                }  
              } 
          }
    }});
    
  if (isFlightDetail) {
    isFlightDetail = false;
   console.log('inidia_departure',contentFlightClick.div_flightid);
   /*
    * untuk menentukan scroll position habis dari menu detail_flight.html
    * */   
   var mm = document.getElementById(contentFlightClick.div_flightid); //
   //element.getElementById('screenFlight').scrollTo(100,0);
   document.getElementById('scrollPanel').scrollToElement(mm); 
  }
  
  if (isShowToast) {
    if (isShowToast) {
      blackberry.ui.toast.show('Tidak Ditemukan');
      isShowToast = false;
    }
  }
  
}

//untuk flight arrival
function storeDataArrival(element,data){
  $('.titleFont').text(titleHeaderFlight);
  $("#tableFlightContent").css("overflow","hidden","position","relative");
  $("#menuFligh"+localStorage.getItem("numberFlight")).attr("data-bb-selected","true");
  $('<div/>',{id:"disablepage",class:"disablepage"}).css({'position':'relative', 'width':'100%','display':'block','height':'1130px','margin-left':'auto','margin-right':'auto','opacity':'0.9','background':'#000'}).appendTo("#screenFlight").hide();
  $('<div/>',{id:"disablefont"}).css({'position':'relative','top':'50%','left':'50%','margin-left':'-100px'}).appendTo("#disablepage");
  $('<font/>',{id:'pulldown',class:'pulldown'}).text('Please Wait..').css({'font-size':'30pt','text-align': 'center',color:'white','font-style':'bold'}).appendTo("#disablefont");
  
  
  new filteredStoreFlight(data,
    function(callback){
       if (callback) {
          var flightItem = JSON.parse(localStorage.getItem("imageStorage"));
          for(var i = 0; i < callback.length ; i++){
                callback[i].from = 'FROM '+callback[i].from;
                callback[i].eta = 'ETA '+callback[i].eta; 
                if (callback[i].reeta == '') {
                      callback[i].reeta = 'REETA -';
                    }else{
                      callback[i].reeta = 'REETA '+callback[i].reeta;
                }
            
                $('<div/>',{id:'idtableflight'+i,class:'list_container',onclick:"showDetailFlight('"+i+"',"+JSON.stringify(callback[i])+")"}).appendTo("#tableFlightContent"); 
                $('<div/>',{id:'idheadtable'+i,class:'container_text'}).appendTo("#idtableflight"+i); 
                $('<font/>',{id:'fromto'+i,class:'fromTo_flightno'}).text(callback[i].from).appendTo("#idheadtable"+i);
                $("<font/>",{id:'idflightNo'+i,class:"fromTo_flightno"}).text(callback[i].flightno).appendTo("#idheadtable"+i);
                $('<div/>',{id:'idheadtable_status'+i,class:'container_text'}).appendTo("#idtableflight"+i);
                $("<font/>",{id:"idstatus"+i,class:"text_status"}).css({color:setColorStatus(callback[i].status)}).text(ifStatusNull(callback[i].status)).appendTo("#idheadtable_status"+i);  
                
               $("<br/>").appendTo("#idheadtable_status"+i);
              $('<div/>',{class:"pagar"}).appendTo("#idheadtable_status"+i);
              $("<div/>",{class:"container_eta_reeta",id:"id_container_eta_reeta"+i}).appendTo("#idheadtable_status"+i);
              $("<font/>",{id:"eta"+i,class:"eta_reeta"}).text(callback[i].eta).appendTo("#id_container_eta_reeta"+i);
              $("<font/>",{id:"reeta"+i,class:"eta_reeta"}).text(callback[i].reeta).appendTo("#id_container_eta_reeta"+i);
          }
      
        var flightitemSaved = JSON.parse(localStorage.getItem("flightSavedStorage"));
      
        if(flightitemSaved){
          for(var a = 0 ; a < flightitemSaved.length; a++){
          console.log(flightitemSaved[a].idflightNo);
            for(var b =0;b<callback.length;b++){
              if(flightitemSaved[a].idflightNo == callback[b].flightno){
                if (callback[b].flightno == contentFlightClick.idflightNo) {
                  if (isFlightDetail) {
                    console.log('inidia',contentFlightClick.div_flightid);
                    /*
                     * untuk menentukan scroll position habis dari menu detail_flight.html
                     * */   
                    var mm = element.getElementById('idtableflight'+(b-1)); //contentFlightClick.div_flightid
                    //element.getElementById('screenFlight').scrollTo(100,0);
                    element.getElementById('scrollPanel').scrollToElement(mm);
                    isFlightDetail = false;
                  }
                }
              $('#idtableflight'+b).hide();
              }
            }  
          }
        }
  
  }});
  
   if (isShowToast) {
    if (isShowToast) {
      isShowToast = false;
      blackberry.ui.toast.show('Tidak Ditemukan');	
    }
  }
  
  if (isFlightDetail) {
    isFlightDetail = false;
    console.log('inidia_arrival',contentFlightClick.div_flightid);
    /*
     * untuk menentukan scroll position habis dari menu detail_flight.html
     * */   
    var mm = document.getElementById(contentFlightClick.div_flightid); //
    //element.getElementById('screenFlight').scrollTo(100,0);
    element.getElementById('scrollPanel').scrollToElement(mm); 
  }
}

function storeDataMoreFlightArrival(data){
  $('#tableFlightContent').html('');
  $('#disablepage').html('');
  var tempMoreData = [];
  var tempMoreDataForList = [];
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));
  var oldDeparture = JSON.parse(localStorage.getItem('storeFlightArrival'));
  new filteredStoreFlight(JSON.parse(localStorage.getItem('storeFlightArrival')),
                  function(callback){
                     if (callback) {
                         for(var a = 0; a<callback.length;a++){
                           tempMoreDataForList.push(callback[a]);
                         }
                         
                         new filteredStoreFlight(data,function(callbackMore){
                                console.log('callback kedua jalan');
                                if (callbackMore) {
                                  for(var a = tempMoreDataForList.length; a<callbackMore.length;a++){
                                    tempMoreDataForList.push(callbackMore[a]);
                                    localStorage.setItem('moreFilterFlightArrival',JSON.stringify(tempMoreDataForList));
                                  }
                                }else{
                                }
                              });
                     }else{
                      
                        new filteredStoreFlight(data,function(callbackMore){
                              console.log('pertama gagal go callback kedua jalan');
                                if (callbackMore) {
                                  for(var a = tempMoreDataForList.length; a<callbackMore.length;a++){
                                    tempMoreDataForList.push(callbackMore[a]);
                                    localStorage.setItem('moreFilterFlightArrival',JSON.stringify(tempMoreDataForList));
                                  }
                                }else{
                                }
                              });
                     }
                  });
  
  for(var a = 0; a<oldDeparture.length;a++){
    tempMoreData.push(oldDeparture[a]);
  }

  for(var a = tempMoreData.length; a<data.length;a++){
    tempMoreData.push(data[a]);
  }
  
  localStorage.setItem('storeFlightArrival',JSON.stringify(tempMoreData)); 
  console.log("data tambah",JSON.parse(localStorage.getItem('storeFlightArrival')));
  
  if (tempMoreDataForList) {
    storeDataArrival(null,tempMoreDataForList);
  }else{
    storeDataArrival(null,JSON.parse(localStorage.getItem('moreFilterFlightArrival')));
  }
  /* end function */
}

function storeDataMoreFlightDeparture(data){
  $('#tableFlightContent').html('');
  $('#disablepage').html('');
  var tempMoreData = [];
  var tempMoreDataForList = [];
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));
  var oldDeparture = JSON.parse(localStorage.getItem('storeFlightDeparture'));
  
  new filteredStoreFlight(JSON.parse(localStorage.getItem('storeFlightDeparture')),function(callback){
      console.log('callback pertama jalan');
        if (callback) {
          for(var a = 0; a<callback.length;a++){
            tempMoreDataForList.push(callback[a]);
            localStorage.setItem('moreFilterFlightDeparture',JSON.stringify(tempMoreDataForList));
          }
          
          new filteredStoreFlight(data,function(callbackMore){
            console.log('callback kedua jalan');
            if (callbackMore) {
              for(var a = tempMoreDataForList.length; a<callbackMore.length;a++){
                tempMoreDataForList.push(callbackMore[a]);
                localStorage.setItem('moreFilterFlightDeparture',JSON.stringify(tempMoreDataForList));
              }
            }else{
            }
          });
          
        }else{
          
         new filteredStoreFlight(data,function(callbackMore){
            console.log('pertama gagal go callback kedua jalan');
            if (callbackMore) {
              for(var a = tempMoreDataForList.length; a<callbackMore.length;a++){
                tempMoreDataForList.push(callbackMore[a]);
                localStorage.setItem('moreFilterFlightDeparture',JSON.stringify(tempMoreDataForList));
              }
            }else{
               
            }
          });
        }
     });
  
  console.log('punya filtered',tempMoreDataForList);
  // JSON.parse(localStorage.getItem('storeFlightDeparture'));
  for(var a = 0; a<oldDeparture.length;a++){
    tempMoreData.push(oldDeparture[a]);
  }
  for(var a =tempMoreData.length; a< data.length;a++){
    tempMoreData.push(data[a]);
  }
  localStorage.setItem('storeFlightDeparture',JSON.stringify(tempMoreData)); 
  console.log("data tambah",JSON.parse(localStorage.getItem('storeFlightDeparture')));
   if (tempMoreDataForList) {
    storeDataDeparture(null,tempMoreDataForList);
  }else{
    storeDataDeparture(null,JSON.parse(localStorage.getItem('moreFilterFlightDeparture')));
  }
    
}

/*
  ====================END storeData========================
*/

/*
  ====================kategory AJAX dalam NAVIGASI========================
*/

function Replay_flightArrival(){
  if (bb.device.isBB10) {
    document.getElementById('bb7Loading').style.display = 'none';
    document.getElementById('bb10Loading').style.visibility = 'visible';
  } else {
    document.getElementById('bb7Loading').style.visibility = 'visible';
    document.getElementById('bb10Loading').style.display = 'none';
  }
  $('#infoAlertBox').hide();
  $("#menuFligh"+localStorage.getItem("numberFlight")).attr("data-bb-selected","true");        
  $.ajax({
      type : 'GET',
      timeout:5000,
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'0/20/'+token,
      dataType : 'json',
      success : function(data) {
          console.log('finish');
          console.log('content:',data);

          if(data){
            document.getElementById('bb7Loading').style.visibility = 'hidden';
            document.getElementById('bb10Loading').style.visibility = 'hidden';
            if(data == ''){
              $('#infoAlertBox').show();
              //$("#infoAlertBox").show();
            }else{
              localStorage.setItem("storeFlightArrival",JSON.stringify(data));
              storeDataArrival(null,data);
            }  
          }else{
            //alert(error);
          }

      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        document.getElementById('bb7Loading').style.visibility = 'hidden';
        document.getElementById('bb10Loading').style.visibility = 'hidden';
        $('#infoAlertBox').show();
      }

  });
}

function Replay_flightDeparture(){
  if (bb.device.isBB10) {
    document.getElementById('bb7Loading').style.display = 'none';
    document.getElementById('bb10Loading').style.display = 'block';
    document.getElementById('bb10Loading').style.visibility = 'visible';
  } else {
    document.getElementById('bb7Loading').style.visibility = 'visible';
    document.getElementById('bb7Loading').style.display = 'block';
    document.getElementById('bb7Loading').style.display = 'none';
  }

  $('#infoAlertBox').hide();
  $("#menuFligh"+localStorage.getItem("numberFlight")).attr("data-bb-selected","true");
    $.ajax({
        type : 'GET',
        timeout:5000,
        url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight')+'0/20/'+token,
        dataType : 'json',
        success : function(data) {
            console.log('finish');
            console.log('content:',data);

            if(data){
              document.getElementById('bb7Loading').style.visibility = 'hidden';
              document.getElementById('bb10Loading').style.visibility = 'hidden';
              if(data == ''){
                $('#infoAlertBox').show();
              }else{
                 localStorage.setItem("storeFlightDeparture",JSON.stringify(data));
                 storeDataDeparture(null,data);
              }
            }else{
              //alert(error);
            }

        },beforeSend: function(){
         console.log('onRunning');
        },error:function(error){
          document.getElementById('bb7Loading').style.visibility = 'hidden';
          document.getElementById('bb10Loading').style.visibility = 'hidden';
           $('#infoAlertBox').show();
        }
    });
}

/*
  ====================END kategory AJAX dalam NAVIGASI========================
*/

function ifStatusNull(data){
  if(data.toUpperCase() == ""){
    return "-";
  }else{
    return data;
  }
}

function reetaIsNull(data){
  if(data){
    return 'REETA '+data;
  }else{
    return '';
  }
}

/*
  ======================== KATEGORY NAVIGASI=======================
*/

function saveFlight(data,index){
  var a = [];
  if(data){ 
    if(localStorage.getItem('flightSavedStorage')){
      var tempFlight = JSON.parse(localStorage.getItem('flightSavedStorage'));
      console.log("tempFlight",tempFlight[0]);
      tempFlight.push(data);
      console.log("jadinya : ",tempFlight);
      localStorage.setItem('flightSavedStorage',JSON.stringify(tempFlight));
      $("#idtableflight"+index).hide();
    }else{
       a.push(data);
      localStorage.setItem('flightSavedStorage',JSON.stringify(a));
      $("#idtableflight"+index).hide();
    }
  }
  console.log(JSON.parse(localStorage.getItem('flightSavedStorage')));
}

function save_flight(){
  var a = [];
  if(document.getElementById('choiceOne').checked){
    if(contentFlightClick){ 
      if(localStorage.getItem('flightSavedStorage')){
        var tempFlight = JSON.parse(localStorage.getItem('flightSavedStorage'));
        console.log("tempFlight",tempFlight[0]);
        tempFlight.push(contentFlightClick);
        console.log("jadinya : ",tempFlight);
        localStorage.setItem('flightSavedStorage',JSON.stringify(tempFlight));
       // $("#idtableflight"+index).hide();
      }else{
        a.push(contentFlightClick);
        localStorage.setItem('flightSavedStorage',JSON.stringify(a));
      //  $("#idtableflight"+index).hide();
      }
    }
  }
  isFlightDetail = true;
  bb.popScreen();
}

var status_flightDetail = [];
var idInterval_detail;
function showDetailFlight(index,flightdata) {
  //edit disini
  console.log(flightdata.to);
  status_flightDetail = {
    id_flight : flightdata.flightno,
    TOKEN : "3b0ce003959c28762881c746fe8e100e6ea8e4ea",
    status_sekarang : flightdata.status
  }
  
  idInterval_detail = window.setInterval(onRefreshStatusDetailFlight,5000);
  
  if (flightdata.to == null) {
    contentFlightClick.fromto = flightdata.from;
  }else{
    contentFlightClick.fromto = flightdata.to;
  }
  
  if (flightdata.gate != null) {
    contentFlightClick.gate = flightdata.gate;
  }else{
    contentFlightClick.gate = '-';
  }
  
  contentFlightClick.div_flightid = 'idtableflight'+index;
  contentFlightClick.idflightNo = flightdata.flightno; 
  contentFlightClick.idstatus = flightdata.status;
  contentFlightClick.idfontETA = flightdata.eta;
  contentFlightClick.idfontREETA = flightdata.reeta;
  bb.pushScreen("detail_flight.html","to_flight_list.html");
}


function onRefreshStatusDetailFlight(){
  
  var urlGetStatus = JSON.parse(localStorage.getItem("urlgetFlightStatus"));
  var urlFlightStatus = '';
  var flightid = [];
  
  if (localStorage.getItem("airportIs") == 'baliairport') {
    urlFlightStatus = urlGetStatus.url[1]._url;
    console.log(urlFlightStatus);
  }else if (localStorage.getItem("airportIs") == 'surabayaairport') {
    urlFlightStatus = urlGetStatus.url[0]._url;
    console.log(urlFlightStatus);
  }
  
  flightid.push(status_flightDetail.id_flight);
  console.log('id_flight',flightid);
  $.ajax({
    type:'POST',
    url : urlFlightStatus,
    dataType:'json',
    data : {
      id_flight : JSON.stringify(flightid),
      TOKEN : status_flightDetail.TOKEN
    },
    success : function(data){
      console.log("khusus detail refresh",data);
      for(var a=0;a<data.length ;a++){
        if (data == '[]') {
          console.log('sudah kosong');
        }else{
          if (data[a].status != status_flightDetail.status_sekarang) {
            $("#status").css("color",setColorStatus(data[i].status)).text(data[a].status);
            break;
          }
        }
      }
      
    },
    error : function(error){
      console.log("error",error);
    }
  });
}



function return_to_listFlight(index){
  //edit disini
  clearInterval(idInterval_detail);
  isFlightDetail = true;
  
}

var a = {};
var indexTableFlight = 0;
function alertFlightSave(data){

  indexTableFlight = data;
  
  //alert($("#fromto"+data).text());
  a = { 
    "fromto" : $("#fromto"+data).text(),
    "idflightNo" : $("#idflightNo"+data).text(),
    "idstatus" : $("#idstatus"+data).text(),
    "gate" : $("#iddivgate"+data).text(),
    "idfontETA" : $("#idfontETA"+data).text(),
    "idfontREETA" : $("#idfontREETA"+data).text(),
    "flightChoice" : localStorage.getItem('urlGetFlight')
  };
  // edit disini
 
  bb.pushScreen("detail_flight.html","detail_flight.html");
  //customDialogSave();

  /*$('<div>').simpledialog2({
    mode: 'button',
    headerText: 'Flight Save',
    headerClose: true,
    buttonPrompt: '<div/><div/><div style="height:35px;"/><p style="margin:10px 10px 10px 10px; font-size:18pt;">Are You Sure to Save Flight Information?</p>',
    buttons : {
      '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >OK</h1>': {
        click: function () { 
          $('#buttonoutput').text('SAVE');
          console.log(JSON.parse(localStorage.getItem('flightSavedStorage')));
          saveFlight(a,data);
        }
      },
      '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >Cancel</h1>': {
        click: function () { 
          $('#buttonoutput').text('Cancel');
        },
        icon: "delete",
        theme: "c"
      }
    }
  })*/
}

function alertDeleteFlight(index){
    indexTableFlight = index;
    customDialogDelete();
            /*
           //bb.pushScreen("flightsaved.html","flightsaved.html");
           $('<div>').simpledialog2({
            mode: 'button',
            headerText: 'Click One...',
            headerClose: true,
            buttonPrompt: '<div/><div/><div style="height:35px;"/><p style="margin:10px 10px 10px 10px; font-size:18pt;">Are You Sure to Delete Flight Information?</p>',
            buttons : {
              '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >OK</h1>': {
                click: function () { 
                  $('#buttonoutput').text('Delete');
                  var a = JSON.parse(localStorage.getItem("flightSavedStorage"));         
                    a.splice(index,1);
                    console.log(a);
                    localStorage.setItem("flightSavedStorage",JSON.stringify(a));
                    $("#idtableflight"+index).remove();         
                }
              },
               '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >Cancel</h1>': {
                click: function () { 
                  $('#buttonoutput').text('Cancel');
                },
                icon: "delete",
                theme: "c"
              }
            }
          })
        */
  }

function deleteAll(){

  for(var a = 0;a<JSON.parse(localStorage.getItem("flightSavedStorage")).length;a++){
     $("#idtableflight"+a).remove();
  }

  localStorage.removeItem('flightSavedStorage');
  localStorage.clear();
  //load kembali image storage kalo enggak di load enggak bisa load data flight
  setImageStorage();
  setPostFlightStatusURL();

}

function customDialogSave() {
  try {
    var buttons = ["Yes", "No"];
    var ops = {title : "Flight Saved", size : blackberry.ui.dialog.SIZE_TALL, position : blackberry.ui.dialog.CENTER};
    blackberry.ui.dialog.customAskAsync("Are You Sure to Save Flight Information?", buttons, dialogCallBackSave, ops);
  } catch(e) {
    alert("Exception in customDialog: " + e);
  }
}

function dialogCallBackSave(index){
  if(index == 0){
    console.log(JSON.parse(localStorage.getItem('flightSavedStorage')));
    saveFlight(a,indexTableFlight);
  }
}

function customDialogDelete() {
  try {
    var buttons = ["Yes", "No"];
    var ops = {title : "Delete Flight", size : blackberry.ui.dialog.SIZE_TALL, position : blackberry.ui.dialog.CENTER};
    blackberry.ui.dialog.customAskAsync("Are You Sure to Delete Flight Information?", buttons, dialogCallBackDelete, ops);
  } catch(e) {
    alert("Exception in customDialog: " + e);
  }
}


function dialogCallBackDelete(index){
  if(index == 0){
    var tempFligSave = JSON.parse(localStorage.getItem("flightSavedStorage"));         
    tempFligSave.splice(indexTableFlight,1);
    console.log(tempFligSave);
    localStorage.setItem("flightSavedStorage",JSON.stringify(tempFligSave));
    $("#idtableflight"+indexTableFlight).remove(); 
  }
}

/*
  ======================== END KATEGORY NAVIGASI=======================
*/


/*
 *======================== KATEGORY FILTER FLIGHT STATUS =================================
 */

function filteredStoreFlight(oriStoreData,callback) {
  this.indexLoop = 0;
  this.isFound = false;  
  this.tempStorData = [];
  if (set_filtered.isFiltered) {
      
    console.log(isMore);
    console.log('kel sekarang',kel);
    console.log('proses sebelum filtering',oriStoreData);
    
    for(var a=0;a < oriStoreData.length;a++){
      if (set_filtered.optionFilteredStatus.toLowerCase() == oriStoreData[a].status.toLowerCase()) {
        this.tempStorData.push(oriStoreData[a]);
        this.isFound = true;
      }
    }
   
    console.log('proses filtering',this.tempStorData);
    if (this.isFound) {
       console.log('isfound',this.isFound);
       callback(this.tempStorData);
        
    }else{
      isShowToast = true;
      console.log('tidak ditemukan');
      //callback(JSON.parse(localStorage.getItem('moreFilterFlightDeparture')));
      callback(oriStoreData);
    }
  }else{
    console.log('kena else');
    callback(oriStoreData);
  }
}

var set_filtered = {
  isFiltered : false,
  optionFilteredStatus : '',
  flightOptionFilteredIs : ''
}

function returnGetFlightFilter(flightIs) {
  isMore = false;
  if (document.getElementById('flightstatus').value == 'none') {
    set_filtered.isFiltered = false;
  }else{
    set_filtered.isFiltered = true;
  }
  set_filtered.optionFilteredStatus = document.getElementById('flightstatus').value;
  set_filtered.flightOptionFilteredIsflightOptionFilteredIs = flightIs;
  bb.popScreen();
  
}

/*
 *======================== END KATEGORY FILTER FLIGHT STATUS =================================
 */


