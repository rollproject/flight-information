/*
  // Create the simplest notification
  new Notification("You have a simple notification");


  // Create a notification with body
  new Notification("The title", {body: "The body"});


  // Create a notification with events
  var title = "You have a notification";
  var options = {
      body : "Some details",
      onshow : function() { alert("The notification was created successfully!"); },
      onerror : function() { alert("The notification could not be created!"); }
  }
  var n = new Notification(title, options);


  // Create notifications with same tag for a chatroom session
  // Bob says "Hi"
  new Notification("Bob: Hi", { tag: 'chat_Bob' });

  // Bob says "Are you free this afternoon?"
  // as only one notification will exist,
  // the application creates another notification with the combined message using the same tag
  new Notification("Bob: Hi / Are you free this afternoon?", { tag: 'chat_Bob' });


  // Create a notification with invocation information
  // and use invoked event to determine and do something for that notification
  var title = "A notification for something";
  var options = {
      targetAction : "bb.action.DoSomethingForNotification", // If no target is specified, it will invoke the current application
      payloadURI : "some link"
  }

  // Create the notification
  new Notification(title, options);
*/

  // Register to listen to invoked event
  
