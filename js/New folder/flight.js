function initApp() {
 bb.pushScreen('main.html', 'main');
}

function hammerTime() {
  var contentSwipe = new Hammer(document.getElementById("content"), {
  });

  contentSwipe.onswipe = function(ev) { 
    var dir = ev.direction;
    
    // swipe from right-to-left
    if (dir === 'left') {
      var currentPage = pageStack[pageStack.length -1];
          currentPage = parseInt(currentPage);
      var nextPage = currentPage + 1;

      if (nextPage >= pages.length + 1) {
        return false;
      } else {
        console.log('loading: ' + nextPage);
        bb.pushScreen(nextPage + '.html', nextPage);
      }

    // swipe from left-to-right
    }  else if (dir === 'right') {
        bb.popScreen()
      }

  };
}

function loadFLightBackgroundProcess(){
  window.setInterval(getFlightStatus,15000);
}


function getFlightStatus(){
  var urlGetStatus = JSON.parse(localStorage.getItem("urlgetFlightStatus"));
  var tempflightno = [];
  console.log(urlGetStatus.url);


  tempflightno = JSON.parse(localStorage.getItem("flightSavedStorage"));
  //console.log(tempflightno);
  var postflightno = [];
  if(tempflightno){
    for(var a = 0;  a < tempflightno.length ; a++){
    postflightno.push(tempflightno[a].idflightNo);
    }

    for(var a = 0 ; a< urlGetStatus.url.length; a++){
        console.log(postflightno);
        $.ajax({
          type : 'POST',
          data: { id_flight : JSON.stringify(postflightno), TOKEN : "3b0ce003959c28762881c746fe8e100e6ea8e4ea"},
          url : urlGetStatus.url[a]._url,
          dataType : 'json',
          timeout: 5000,
          success : function(data) {
            //alert(data);
            flightcheckstatus(data);
              console.log("data",data);
          },error:function(error){
          }
        });
     } 
  }
}

function flightcheckstatus(data){
  var nowflightstatus = JSON.parse(localStorage.getItem('flightSavedStorage'));
  for(var a = 0; a< data.length ; a++){
    for(var i = 0; i < nowflightstatus.length ; i++){
      if(data[a].status != ""){
        if(data[a].flightno == nowflightstatus[i].idflightNo){
          if(data[a].status != nowflightstatus[i].idstatus){
            store_notification(data[a]);
            nowflightstatus[i].idstatus = data[a].status;
            localStorage.setItem("flightSavedStorage",JSON.stringify(nowflightstatus));
            break;
            //console.log(data[a].status,"blablabla")
          }
        }
      }     
    }    
  }

  if(localStorage.getItem("storageNotification")){
    console.log("dialog keluar",JSON.parse(localStorage.getItem("storageNotification")));
    var title,notification,option;
    options = {
      body:     "blablabla",
      tag:      "blablablablabla",
      target:     "babibubebebfvjfvf",
      targetAction: "vflvflvflvf",
      payload:    "vflvflvflvfl",
      payloadURI:   "vfwefwffvvdf",
      payloadType:  "29mcqsakocmsfkocmfkc",
      onclick:   " _notify.onclick",
      onshow:     "_notify.onshow",
      onerror:    "_notify.onerror",
      onclose:    "_notify.onclose"
    };
    notification = new Notification(title, options);
  }
}

function store_notification(data){

  var tempStore = JSON.parse(localStorage.getItem("storageNotification"));
  
  var tempNotif = {
    flightid : data.flightno,
    status : data.status
  };

  if(localStorage.getItem("storageNotification")){
    
    var isSame = false;
    for(var a = 0; a< tempStore.length;a++){
      if(tempStore[a].flightid == data.flightno && tempStore[a].status != data.status){
        tempStore.splice(a);
        //localStorage.setItem("storageNotification",JSON.stringify(tempStore));
        tempStore.push(tempNotif);
        localStorage.setItem("storageNotification",JSON.stringify(tempStore));
        isSame = true;
        break;
      }else{
        isSame = false;
      }
    }  
    if(!isSame){
        tempStore.push(tempNotif);
        localStorage.setItem("storageNotification",JSON.stringify(tempStore));   
    }
  }else{
    var toSave = [];
    toSave.push(tempNotif);
    localStorage.setItem("storageNotification",JSON.stringify(toSave));
    

  }
}

function flightsaved(){
  bb.pushScreen("flightsaved.html","flightsaved.html");
}

function intentTo(ev,optionUrl,optionUrlContent){
  if(optionUrl == "baliairport"){ 
    localStorage.setItem('urlGetFlight',baliairport);
  }else{
    localStorage.setItem('urlGetFlight',surabayaairport);
  }
  localStorage.setItem('airportIs',optionUrl);
  localStorage.setItem('urlContentFlight',optionUrlContent); 
  if(ev){
    bb.pushScreen(ev,ev);
  }else{
  }
  console.log(localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight'));
};

function sub_intentTo(ev,optionUrlContent){
  localStorage.setItem('urlContentFlight',optionUrlContent); 
  if(ev){
    bb.pushScreen(ev,ev);
  }else{
  }
  console.log(localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight'));
};


function FlightArrival_initialLoad(element){
  // Make the proper activity indicator appear
  if (bb.device.isBB10) {
    element.getElementById('bb7Loading').style.display = 'none';
  } else {
    element.getElementById('bb10Loading').style.display = 'none';
  }
  $.ajax({
      type : 'GET',
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight'),
      dataType : 'json',
      success : function(data) {
          element.getElementById('tableFlightContent').style.display = 'inline'; 
          element.getElementById('bb7Loading').style.visibility = 'hidden';
          element.getElementById('bb10Loading').style.visibility = 'hidden';
          element.getElementById('tableFlightContent').style.visibility = 'visible';
          console.log('finish');
          console.log('content:',data);

          if(data){
            storeDataArrival(element,data);
          }else{
            //alert(error);
          }

      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        alert(error);
      }

  });
}

function FlightDeparture_initialLoad(element){
  // Make the proper activity indicator appear
  if (bb.device.isBB10) {
    element.getElementById('bb7Loading').style.display = 'none';
  } else {
    element.getElementById('bb10Loading').style.display = 'none';
  }

  $.ajax({
      type : 'GET',
      url : localStorage.getItem('urlGetFlight')+localStorage.getItem('urlContentFlight'),
      dataType : 'json',
      success : function(data) {
          element.getElementById('tableFlightContent').style.display = 'inline'; 
          element.getElementById('bb7Loading').style.visibility = 'hidden';
          element.getElementById('bb10Loading').style.visibility = 'hidden';
          element.getElementById('tableFlightContent').style.visibility = 'visible';
          console.log('finish');
          console.log('content:',data);

          if(data){
            storeDataDeparture(element,data);
          }else{
            //alert(error);
          }

      },beforeSend: function(){
       console.log('onRunning');
      },error:function(error){
        alert(error);
      }

  });
}

function FlightSaved_initialLoad(element){
 if (bb.device.isBB10) {
    element.getElementById('bb7Loading').style.display = 'none';
  } else {
    element.getElementById('bb10Loading').style.display = 'none';
  }
  if(localStorage.getItem("flightSavedStorage")){
    storeDataFlightSaved(element);
  }else{
    element.getElementById('bb7Loading').style.visibility = 'hidden';
    element.getElementById('bb10Loading').style.visibility = 'hidden';
  }
}

function FlightArrival_onScreenReady(element){
  
  element.getElementById('tableFlightContent').style.display = 'none';       
  element.getElementById('tableFlightContent').style.visibility = 'hidden';
  element.getElementById('bb7Loading').style.visibility = 'visible';
  element.getElementById('bb10Loading').style.visibility = 'visible';

                
  
}

function FlightDeparture_onScreenReady(element){

  element.getElementById('tableFlightContent').style.display = 'none';       
  element.getElementById('tableFlightContent').style.visibility = 'hidden';
  element.getElementById('bb7Loading').style.visibility = 'visible';
  element.getElementById('bb10Loading').style.visibility = 'visible';
               
  

}

function sleep(milliseconds) {
var start = new Date().getTime();
while ((new Date().getTime() - start) < milliseconds){
// Do nothing
}
}

function FlightSaved_onScreenReady(element){
  element.getElementById('tableFlightContent').style.display = 'inline';       
  element.getElementById('tableFlightContent').style.visibility = 'visible';
  element.getElementById('bb7Loading').style.visibility = 'visible';
  element.getElementById('bb10Loading').style.visibility = 'visible';

  if(localStorage.getItem('flightSavedStorage')){
    element.getElementById('bb7Loading').style.visibility = 'visible';
    element.getElementById('bb10Loading').style.visibility = 'visible';
  }else{
    element.getElementById('bb7Loading').style.visibility = 'hidden';
    element.getElementById('bb10Loading').style.visibility = 'hidden';
  }
}

/*
SAMPLE 
<div class="tableflight" id="idtableflight">
      <div class="headtable"><font class="fontFlight" id="from">To BANJARMASIN</font></div>
      <img src="images/asset/vf_t.png" class="image_flight">
      <font class="flightNo" id="idflightNo">JT 008</font>
      <font class="status" id="idstatus">LANDED</font>
      <div class="nextButton"><img src="images/Action_Icons_BlackBerry_10/ic_next.png" class="btnnext"  ></div>
      <div class="foottable"><font class="fontFlight">ETA 10:50</font><font class="fontREETA">RE ETA 10:50</font></div>
    </div>
    */

function storeDataFlightSaved(element){

 element.getElementById('bb7Loading').style.visibility = 'hidden';
 element.getElementById('bb10Loading').style.visibility = 'hidden';
  
 var flightItem = JSON.parse(localStorage.getItem("imageStorage"));

 var data = JSON.parse(localStorage.getItem("flightSavedStorage"));
 

 console.log("data",data);

  for(var i = 0; i < data.length ; i++){      
    $('<div/>',{id:'idtableflight'+i,class:'tableflight',onclick:"alertDeleteFlight('"+i+"')"}).appendTo("#tableFlightContent"); 
    $('<div/>',{id:'idheadtable'+i,class:'headtable'}).appendTo("#idtableflight"+i); 
    $('<font/>',{id:'fromto'+i,class:'fontFlight'}).text(data[i].fromto).appendTo("#idheadtable"+i);
    if(flightItem[data[i].idflightNo.substring(0,2).toLowerCase()] == null){
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem["unknown"]}).appendTo("#idtableflight"+i);
    }else{
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem[data[i].idflightNo.substring(0,2).toLowerCase()]}).appendTo("#idtableflight"+i);
    }
    $("<font/>",{id:'idflightNo'+i,class:"flightNo"}).text(data[i].idflightNo).appendTo("#idtableflight"+i);
    $('<div/>',{id:'iddivgate'+i,class:'divgate'}).appendTo("#idtableflight"+i);
    $('<font/>',{id:"idgate"+i,class:'gate'}).text(data[i].gate).appendTo("#iddivgate"+i);
    $("<font/>",{id:"idstatus"+i,class:"status"}).css({color:setColorStatus(data[i].idstatus)}).text(ifStatusNull(data[i].idstatus)).appendTo("#idtableflight"+i);
    $('<div/>',{id:'idnextButton'+i,class:'nextButton'}).appendTo("#idtableflight"+i);
    $('<img/>',{id:'idbtnnext'+i,class:'btnnext',src:'images/Action_Icons_BlackBerry_10/ic_next.png'}).appendTo("#idnextButton"+i);
    $('<div/>',{id:'idfoottable'+i,class:'foottable'}).appendTo('#idtableflight'+i);
    $('<font/>',{id:'idfontETA'+i,class:'fontFlight'}).text(data[i].idfontETA).appendTo("#idfoottable"+i);
    $('<font/>',{id:'idfontREETA'+i,class:'fontREETA'}).text(data[i].idfontREETA).appendTo("#idfoottable"+i);  
  }
}

//untuk flight arrival
function storeDataArrival(element,data){
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));

  for(var i = 0; i < data.length ; i++){   
    $('<div/>',{id:'idtableflight'+i,class:'tableflight',onclick:"alertFlightSave('"+i+"')"}).appendTo("#tableFlightContent"); 
    $('<div/>',{id:'idheadtable'+i,class:'headtable'}).appendTo("#idtableflight"+i); 
    $('<font/>',{id:'fromto'+i,class:'fontFlight'}).text('FROM '+data[i].from).appendTo("#idheadtable"+i);
    if(flightItem[data[i].flightno.substring(0,2).toLowerCase()] == null){
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem["unknown"]}).appendTo("#idtableflight"+i);
    }else{
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem[data[i].flightno.substring(0,2).toLowerCase()]}).appendTo("#idtableflight"+i);
    }
    $("<font/>",{id:'idflightNo'+i,class:"flightNo"}).text(data[i].flightno).appendTo("#idtableflight"+i);
    $("<font/>",{id:"idstatus"+i,class:"status"}).css({color:setColorStatus(data[i].status)}).text(ifStatusNull(data[i].status)).appendTo("#idtableflight"+i);
    $('<div/>',{id:'idnextButton'+i,class:'nextButton'}).appendTo("#idtableflight"+i);
    $('<img/>',{id:'idbtnnext'+i,class:'btnnext',src:'images/Action_Icons_BlackBerry_10/ic_next.png'}).appendTo("#idnextButton"+i);
    $('<div/>',{id:'idfoottable'+i,class:'foottable'}).appendTo('#idtableflight'+i);
    $('<font/>',{id:'idfontETA'+i,class:'fontFlight'}).text('ETA '+data[i].eta).appendTo("#idfoottable"+i);
    $('<font/>',{id:'idfontREETA'+i,class:'fontREETA'}).text(reetaIsNull(data[i].reeta)).appendTo("#idfoottable"+i);


  var flightitemSaved = JSON.parse(localStorage.getItem("flightSavedStorage"));
  
    if(flightitemSaved){
      for(var a = 0 ; a < flightitemSaved.length; a++){
      console.log(flightitemSaved[a].idflightNo);
      if(flightitemSaved[a].idflightNo == data[i].flightno){
        $('#idtableflight'+i).hide();
      }
      }
    }    
  
}
}

function storeDataDeparture(element,data){
  var flightItem = JSON.parse(localStorage.getItem("imageStorage"));
 
  for(var i = 0; i < data.length ; i++){   

    $('<div/>',{id:'idtableflight'+i,class:'tableflight',onclick:"alertFlightSave('"+i+"')"}).appendTo("#tableFlightContent"); 
    $('<div/>',{id:'idheadtable'+i,class:'headtable'}).appendTo("#idtableflight"+i); 
    $('<font/>',{id:'fromto'+i,class:'fontFlight'}).text('TO '+data[i].to).appendTo("#idheadtable"+i);
    if(flightItem[data[i].flightno.substring(0,2).toLowerCase()] == null){
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem["unknown"]}).appendTo("#idtableflight"+i);
    }else{
      $('<img/>',{id:'idimage_flight'+i,class:'image_flight',src:flightItem[data[i].flightno.substring(0,2).toLowerCase()]}).appendTo("#idtableflight"+i);
    }
    $("<font/>",{id:'idflightNo'+i,class:"flightNo"}).text(data[i].flightno).appendTo("#idtableflight"+i);
    $('<div/>',{id:'iddivgate'+i,class:'divgate'}).appendTo("#idtableflight"+i);
    $('<font/>',{id:"idgate"+i,class:'gate'}).text(data[i].gate).appendTo("#iddivgate"+i);
    $("<font/>",{id:"idstatus"+i,class:"status"}).css({color:setColorStatus(data[i].status)}).text(ifStatusNull(data[i].status)).appendTo("#idtableflight"+i); 
    $('<div/>',{id:'idnextButton'+i,class:'nextButton'}).appendTo("#idtableflight"+i);
    $('<img/>',{id:'idbtnnext'+i,class:'btnnext',src:'images/Action_Icons_BlackBerry_10/ic_next.png'}).appendTo("#idnextButton"+i);
    $('<div/>',{id:'idfoottable'+i,class:'foottable'}).appendTo('#idtableflight'+i);
    $('<font/>',{id:'idfontETA'+i,class:'fontFlight'}).text('ETA '+data[i].eta).appendTo("#idfoottable"+i);
    $('<font/>',{id:'idfontREETA'+i,class:'fontREETA'}).text(reetaIsNull(data[i].reeta)).appendTo("#idfoottable"+i);

    var flightitemSaved = JSON.parse(localStorage.getItem("flightSavedStorage"));
      if(flightitemSaved){
        for(var a = 0 ; a < flightitemSaved.length; a++){
        console.log(flightitemSaved[a].idflightNo);
        if(flightitemSaved[a].idflightNo == data[i].flightno){
          $('#idtableflight'+i).hide();
        }
    }
    
    }
    
    
  }
}

function ifStatusNull(data){
  if(data.toUpperCase() == ""){
    return "-";
  }else{
    return data;
  }
}

function reetaIsNull(data){
  if(data){
    return 'REETA '+data;
  }else{
    return '';
  }
}

function saveFlight(data,index){
  var a = [];
  if(data){ 
    if(localStorage.getItem('flightSavedStorage')){
      var tempFlight = JSON.parse(localStorage.getItem('flightSavedStorage'));
      console.log("tempFlight",tempFlight[0]);
      tempFlight.push(data);
      console.log("jadinya : ",tempFlight);
      localStorage.setItem('flightSavedStorage',JSON.stringify(tempFlight));
      $("#idtableflight"+index).hide();
    }else{
       a.push(data);
      localStorage.setItem('flightSavedStorage',JSON.stringify(a));
      $("#idtableflight"+index).hide();
    }
  }
  console.log(JSON.parse(localStorage.getItem('flightSavedStorage')));
}


function alertFlightSave(data){
  //alert($("#fromto"+data).text());
  var a = { 
    "fromto" : $("#fromto"+data).text(),
    "idflightNo" : $("#idflightNo"+data).text(),
    "idstatus" : $("#idstatus"+data).text(),
    "gate" : $("#iddivgate"+data).text(),
    "idfontETA" : $("#idfontETA"+data).text(),
    "idfontREETA" : $("#idfontREETA"+data).text(),
    "flightChoice" : localStorage.getItem('urlGetFlight')
  };

  $('<div>').simpledialog2({
    mode: 'button',
    headerText: 'Click One...',
    headerClose: true,
    buttonPrompt: '<div/><div/><div style="height:35px;"/><p style="margin:10px 10px 10px 10px; font-size:18pt;">Are You Sure to Save Flight Information?</p>',
    buttons : {
      '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >OK</h1>': {
        click: function () { 
          $('#buttonoutput').text('SAVE');
          console.log(JSON.parse(localStorage.getItem('flightSavedStorage')));
          saveFlight(a,data);
        }
      },
      '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >Cancel</h1>': {
        click: function () { 
          $('#buttonoutput').text('Cancel');
        },
        icon: "delete",
        theme: "c"
      }
    }
  })
}

function alertDeleteFlight(index){
  
   //bb.pushScreen("flightsaved.html","flightsaved.html");
   $('<div>').simpledialog2({
    mode: 'button',
    headerText: 'Click One...',
    headerClose: true,
    buttonPrompt: '<div/><div/><div style="height:35px;"/><p style="margin:10px 10px 10px 10px; font-size:18pt;">Are You Sure to Delete Flight Information?</p>',
    buttons : {
      '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >OK</h1>': {
        click: function () { 
          $('#buttonoutput').text('SAVE');
          var a = JSON.parse(localStorage.getItem("flightSavedStorage"));         
            a.splice(index);
            console.log(a);
            localStorage.setItem("flightSavedStorage",JSON.stringify(a));
            $("#idtableflight"+index).remove();         
        }
      },
       '<h1 style="font-size:24pt; padding-top:0px;" align="center"  >Cancel</h1>': {
        click: function () { 
          $('#buttonoutput').text('Cancel');
        },
        icon: "delete",
        theme: "c"
      }
    }
  })
}

function deleteAll(){
  for(var a = 0;a<JSON.parse(localStorage.getItem("flightSavedStorage")).length;a++){
     $("#idtableflight"+a).remove();
  }
  localStorage.clear();
  //load kembali image storage kalo enggak di load enggak bisa load data flight
  setImageStorage();
  setPostFlightStatusURL();

}





