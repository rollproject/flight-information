var baliairport = 'http://oprekberry.com/BaliAirportWS/index.php/info/';
var surabayaairport = 'http://oprekberry.com/BaliAirportWS/index.php/info/info_sub/';
var token = '3b0ce003959c28762881c746fe8e100e6ea8e4ea';
var subArrivalUrlContentDomestic = 'getInfo/domestic/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea';
var subDepartureUrlContentDomestic = 'getInfo/domestic/departure/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea'
var subArrivalUrlContentInternational = 'getInfo/international/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea';
var subDepartureUrlContentInternational = 'getInfo/international/departure/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea';


function setPostFlightStatusURL(){
	var urlgetFlightStatus = {"url" : [
	{
		_url : "http://oprekberry.com/BaliAirportWS/index.php/info/info_sub/getFlightStatus"
	},
	{ 
		_url : "http://oprekberry.com/BaliAirportWS/index.php/info/getFlightStatus"
	}
	]};
	
	localStorage.setItem("urlgetFlightStatus", JSON.stringify(urlgetFlightStatus));
}

function setTempFlightStorage(){
	
	
}

function setImageStorage(){
	var imageStorage = { 
			"unknown":"images/asset/airplane_unknown.png",
			"aa":"images/asset/aa_t.png",
			"af":"images/asset/afe_t.png",
			"ak":"images/asset/ak_t.png",
			"avi":"images/asset/avi_t.png",
			"ba":"images/asset/ba_t.png",
			"br":"images/asset/br_t.png",
			"ci":"images/asset/ci_t.png",
			"cx":"images/asset/cx_t.png",
			"dj":"images/asset/dj_t.png",
			"fd":"images/asset/fd_t.png",
			"ga":"images/asset/ga_t.png",
			"gct":"images/asset/gct_t.png",
			"iw":"images/asset/iw_t.png",
			"jl":"images/asset/jl_t.png",
			"jq":"images/asset/jq_t.png",
			"jt":"images/asset/jt_t.png",
			"ke":"images/asset/ke_t.png",
			"kl":"images/asset/kl_t.png",
			"mh":"images/asset/mh_t.png",
			"mu":"images/asset/mu_t.png",
			"mz":"images/asset/mz_t.png",
			"pr":"images/asset/pr_t.png",
			"qf":"images/asset/qf_t.png",
			"qr":"images/asset/qr_t.png",
			"qz":"images/asset/qz_t.png",
			"ri":"images/asset/ri_t.png",
			"sj":"images/asset/sj_t.png",
			"sq":"images/asset/sq_t.png",
			"su":"images/asset/su_t.png",
			"sy":"images/asset/sy_t.png",
			"tg":"images/asset/tg_t.png",
			"tra":"images/asset/tra_t.png",
			"un":"images/asset/un_t.png",
			"va":"images/asset/va_t.png",
			"vf":"images/asset/vf_t.png",
			"xr":"images/asset/xr_t.png",
			"y6":"images/asset/y6_t.png"
	}	
	localStorage.setItem("imageStorage",JSON.stringify(imageStorage));
}


/*
kalo url content ternyata beda beda pake ini
sasaran adalah method function "sub_intentTo"
*/
function setUrlContent(){
	var urlStorage = {
		"baliairport" : [{
			"publicURL" :  "http://oprekberry.com/BaliAirportWS/index.php/info/getInfo/",
			"ArrivalUrlContentDomestic":"domestic/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea",
			"DepartureUrlContentDomestic" :"domestic/departure/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea",
			"ArrivalUrlContentInternational" : "international/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea",
			"DepartureUrlContentInternational" : "international/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea"
		}],
		"surabayaairport" : [{
			"publicURL" :  "http://oprekberry.com/BaliAirportWS/index.php/info/info_sub/",
			"ArrivalUrlContentDomestic":"domestic/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea",
			"DepartureUrlContentDomestic" :"domestic/departure/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea",
			"ArrivalUrlContentInternational" : "international/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea",
			"DepartureUrlContentInternational" : "international/arrival/0/10/3b0ce003959c28762881c746fe8e100e6ea8e4ea"
		}]

	}
}

function setColorStatus(data){
  if(data.toUpperCase() == "LANDED".toUpperCase()){
    return "#00FF00";
  }else if(data.toUpperCase() == "NO OPERATE".toUpperCase()){
    return "#D61F1F";
  }else if(data.toUpperCase() == "DELAYED".toUpperCase()){
    return "#FFFF00";
  }else if(data.toUpperCase() == "WAITINGROOM"){
    return "#FFFF00";
  }else if(data.toUpperCase() == "CHECK IN"){
    return "#39D749";
  }else if(data.toUpperCase() == ""){
    return "#ff0";
  }else if(data.toUpperCase() == "CANCELLED"){
    return "#FFFF00";
  }else if(data.toUpperCase() == "BOARDING"){
    return "#FFA07A";
  }else if(data.toUpperCase() == "FINAL CALL"){
    return "#FFFF00";
  }else if(data.toUpperCase() == "DEPARTED"){
    return "#39D749";
  }else if(data.toUpperCase() == "LATE ARR"){
    return "#FFFF00";
  }else if(data.toUpperCase() == "OPEN"){
    return "#ADFF2F";
  }else{
    return "#FFFF00";
  }
}